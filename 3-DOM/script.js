'use strict';

function mostrarReloj() {
  // Elemento "Date" (Fecha y hora)
  let date = new Date();

  // Elemento HTML "body", que configuramos con
  // color de fondo negro con CSS
  const body = document.querySelector('body');
  body.style.background = 'black';

  // Elemento HTML "div" creado, que vamos a incluir
  // como hijo del elemento HTML "body", con una
  // configuración de estilo (CSS) muy sencilla
  const reloj = document.createElement('div');
  reloj.style.backgroundColor = 'green';
  reloj.style.height = '30rem';
  reloj.style.display = 'flex';
  reloj.style.alignItems = 'center';

  // Elemento HTML "ul" (lista desordenada) creado,
  // que vamos a incluir como hijo del elemento HTML
  // "div" (var. "reloj")
  const lista = document.createElement('ul');

  // Elemento HTML "li" (línea de lista) creado, que
  // vamos a incluir como hijo del elemento HTML "ul"
  // (var. lista), con una configuración de estilo
  // (CSS) mínima
  const item = document.createElement('li');
  item.style.fontSize = '15rem';
  item.style.padding = '2rem';
  item.style.color = 'white';
  // Fragmento de la fecha y hora que nos interesa,
  // la hora, en formato HH:MM:SS
  item.textContent = date.toISOString().slice(11, 19);

  // Se añade elemento "li" como hijo a "ul"
  lista.appendChild(item);

  // Se añade elemento "ul" como hijo a "div"
  reloj.appendChild(lista);

  // Se añade elemento "div" como hijo a "body"
  body.appendChild(reloj);

  // Función que ejecuta un "callback" cada cierto intervalo
  // de tiempo, en concreto, 1 seg. (1 seg. = 1000 ms)
  setInterval(() => {
    date = new Date();
    item.textContent = date.toISOString().slice(11, 19);
  }, 1000);
}

// Llamada a la función que inicia el script
mostrarReloj();
