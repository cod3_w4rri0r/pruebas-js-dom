'use strict';

function juegoAdivinar() {
  // CONTRASEÑA A ADIVINAR
  const numero = Math.floor(101 * Math.random());
  // Multiplicamos por 101, aunque el rango de contraseña
  // es 0 - 100, para que el número 100 tenga la misma
  // probabilidad de ser escogido que el resto de números
  // al redondear al entero inferior más próximo (Math.floor())

  console.log(numero); // Para saber contraseña y testear función
  let v = 0;

  for (let i = 0; i < 5; i++) {
    v = parseInt(prompt('Elige un número del 0 al 100, por favor'));
    if (v === numero) {
      alert(`¡Enhorabuena! Has ganado, la contraseña era ${numero}`);
      return;
    } else {
      alert(`Has fallado, la contraseña es ${v > numero ? 'menor' : 'mayor'}`);
    }
  }
  alert(
    'Game Over! Lo siento, has perdido, sólo contabas con 5 intentos para adivinar la contraseña'
  );
}

juegoAdivinar();
