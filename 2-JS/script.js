'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

// Función que muestra el nombre y puntuación del
// equipo con máxima y mínima puntuación:

function mostrarEquiposMaxMin(objetos) {
  const puntos = [];
  for (let i = 0; i < objetos.length; i++) {
    puntos[i] = objetos[i].puntos.reduce((acc, p) => (acc += p));
  }
  const indiceMax = puntos.indexOf(Math.max(...puntos));
  const indiceMin = puntos.indexOf(Math.min(...puntos));
  console.log(
    `El equipo con la máxima puntuación, ${puntos[indiceMax]}, es ${puntuaciones[indiceMax].equipo}.`
  );
  console.log(
    `Y el equipo con la mínima puntuación, ${puntos[indiceMin]}, es ${puntuaciones[indiceMin].equipo}.`
  );
}

mostrarEquiposMaxMin(puntuaciones);
